package main.webapp;

import main.entities.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FlightServlet extends HttpServlet {

    private FlightDAO flightDAO;

    public FlightServlet() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");

        String number = request.getParameter("number");
        String airplaneType = request.getParameter("airplane_type");
        String departureCity = request.getParameter("departure_city");
        String departureTime = request.getParameter("departure_time");
        String arrivalCity = request.getParameter("arrival_city");
        String arrivalTime = request.getParameter("arrival_time");
        Date departure = null;
        Date arrival = null;
        try {
            departure = new SimpleDateFormat("dd/MM/yyyy").parse(departureTime);
            arrival = new SimpleDateFormat("dd/MM/yyyy").parse(arrivalTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Flight flight = new Flight();
        flight.setNumber(number);
        flight.setAirplaneType(airplaneType);
        flight.setDepartureCity(departureCity);
        flight.setDepartureTime(departure);
        flight.setArrivalCity(arrivalCity);
        flight.setArrivalTime(arrival);

        flightDAO.addFlight(flight);

        request.getRequestDispatcher("flights.html").include(request, response);

    }

    public void doPut(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");

        String sid = request.getParameter("id");
        int id = Integer.parseInt(sid);
        String number = request.getParameter("number");
        String airplaneType = request.getParameter("airplane_type");
        String departureCity = request.getParameter("departure_city");
        String departureTime = request.getParameter("departure_time");
        String arrivalCity = request.getParameter("arrival_city");
        String arrivalTime = request.getParameter("arrival_time");
        Date departure = null;
        Date arrival = null;
        try {
            departure = new SimpleDateFormat("dd/MM/yyyy").parse(departureTime);
            arrival = new SimpleDateFormat("dd/MM/yyyy").parse(arrivalTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Flight flight = new Flight();
        flight.setId(id);
        flight.setNumber(number);
        flight.setAirplaneType(airplaneType);
        flight.setDepartureCity(departureCity);
        flight.setDepartureTime(departure);
        flight.setArrivalCity(arrivalCity);
        flight.setArrivalTime(arrival);

        flightDAO.editFlight(flight);

    }

    public void doDelete(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String sid = request.getParameter("id");
        int id = Integer.parseInt(sid);
        String number = request.getParameter("number");
        String airplaneType = request.getParameter("airplane_type");
        String departureCity = request.getParameter("departure_city");
        String departureTime = request.getParameter("departure_time");
        String arrivalCity = request.getParameter("arrival_city");
        String arrivalTime = request.getParameter("arrival_time");
        Date departure = null;
        Date arrival = null;
        try {
            departure = new SimpleDateFormat("dd/MM/yyyy").parse(departureTime);
            arrival = new SimpleDateFormat("dd/MM/yyyy").parse(arrivalTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Flight flight = new Flight();
        flight.setId(id);
        flight.setNumber(number);
        flight.setAirplaneType(airplaneType);
        flight.setDepartureCity(departureCity);
        flight.setDepartureTime(departure);
        flight.setArrivalCity(arrivalCity);
        flight.setArrivalTime(arrival);

        flightDAO.deleteFlight(flight);

    }

}
