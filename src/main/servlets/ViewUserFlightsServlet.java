package main.servlets;

import main.dao.FlightDAO;
import main.dao.UserDAO;
import main.entities.Flight;
import main.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ViewUserFlightsServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            String username = cookies[0].getValue();
            User user = UserDAO.findUser(username);
            if (!username.equals("") && user.getRole().equals("user")) {
                request.getRequestDispatcher("userprofile.html").include(request, response);

                out.println("<h1>Flights</h1>");

                List<Flight> flights = FlightDAO.findFlights();

                out.println("<table border='1' width='100%'>");
                out.println("<tr>" +
                        "<th>ID</th>" +
                        "<th>Number</th>" +
                        "<th>Airplane type</th>" +
                        "<th>Departure city</th>" +
                        "<th>Departure date</th>" +
                        "<th>Departure time</th>" +
                        "<th>Arrival city</th>" +
                        "<th>Arrival date</th>" +
                        "<th>Arrival time</th>" +
                        "</tr>");

                for (Flight flight : flights) {
                    out.println("<tr>" +
                            "<td>" + flight.getId() +
                            "</td><td>" + flight.getNumber() +
                            "</td><td>" + flight.getAirplaneType() +
                            "</td><td>" + flight.getDepartureCity().getCity_name() +
                            "</td><td>" + flight.getDepartureDate() +
                            "</td><td>" + flight.getDepartureTime() +
                            "</td><td>" + flight.getArrivalCity().getCity_name() +
                            "</td><td>" + flight.getArrivalDate() +
                            "</td><td>" + flight.getArrivalTime() +
                            "</td>" +
                            "</tr>");
                }

                out.println("</table>");

            } else if (!username.equals("") && user.getRole().equals("admin")) {
                request.getRequestDispatcher("adminprofile.html").include(request, response);
                out.println("<br/>You don't have enough privileges!");
            } else {
                out.println("Please login first!");
                request.getRequestDispatcher("index.html").include(request, response);
            }
        }

    }

}
