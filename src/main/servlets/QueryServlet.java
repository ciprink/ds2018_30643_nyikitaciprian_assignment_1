package main.servlets;

import jdk.internal.org.xml.sax.InputSource;
import jdk.internal.org.xml.sax.SAXException;
import main.dao.CityDAO;
import main.dao.FlightDAO;
import main.dao.UserDAO;
import main.entities.City;
import main.entities.Flight;
import main.entities.User;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.*;

public class QueryServlet extends HttpServlet {
 
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {



        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            String username = cookies[0].getValue();
            User user = UserDAO.findUser(username);
            if (!username.equals("") && user.getRole().equals("user")) {

                request.getRequestDispatcher("query.html").include(request, response);

                out.println("<h1>Cities</h1>");

                List<City> cities = CityDAO.findCities();

                out.println("<table border='1' width='100%'>");
                out.println("<tr>" +
                        "<th>ID</th>" +
                        "<th>Name</th>" +
                        "<th>Latitude</th>" +
                        "<th>Longitude</th>" +
                        "</tr>");

                for (City ct : cities) {
                    out.println("<tr>" +
                            "</td><td>" + ct.getId() +
                            "</td><td>" + ct.getCity_name() +
                            "</td><td>" + ct.getLatitude() +
                            "</td><td>" + ct.getLongitude() +
                            "</td>" +
                            "</tr>");
                }

                out.println("</table>");

                String city_param = request.getParameter("city");
                City city = CityDAO.findCity(city_param);

        // -------------------------------------------------------------------------------------------------------------------------

                String url = "http://www.new.earthtools.org/timezone-1.1/" + city.getLatitude() + "/" + city.getLongitude();

                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                // optional default is GET
                con.setRequestMethod("GET");

                //add request header
                // con.setRequestProperty("User-Agent", USER_AGENT);

                int responseCode = con.getResponseCode();
                System.out.println("\nSending 'GET' request to URL : " + url);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer buffer_response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    buffer_response.append(inputLine);
                }
                in.close();

                String response_string = buffer_response.toString();
                System.out.println(response_string);

                int startindex = response_string.indexOf("<localtime>");
                int endindex = response_string.indexOf("</localtime>");

                System.out.println(startindex + " - " + endindex);

                out.println("The local time in "+ city_param + " is " + response_string.substring(startindex, endindex));

               // final String str = "<tag>apple</tag><b>hello</b><tag>orange</tag><tag>pear</tag>";
                // out.println(Arrays.toString(getTagValues(response_string).toArray()));



            } else if (!username.equals("") && user.getRole().equals("admin")) {
                request.getRequestDispatcher("adminprofile.html").include(request, response);
                out.println("<br/>You don't have enough privileges!");
            } else {
                out.println("Please login first!");
                request.getRequestDispatcher("index.html").include(request, response);
            }
        }
    }

    private static final Pattern TAG_REGEX = Pattern.compile("<tag>(.+?)</tag>", Pattern.DOTALL);

    private static List<String> getTagValues(final String str) {
        final List<String> tagValues = new ArrayList<String>();
        final Matcher matcher = TAG_REGEX.matcher(str);
        while (matcher.find()) {
            tagValues.add(matcher.group(1));
        }
        return tagValues;
    }

}
