package main.servlets;

import main.dao.CityDAO;
import main.dao.FlightDAO;
import main.dao.UserDAO;
import main.entities.City;
import main.entities.Flight;
import main.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddFlightServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            String username = cookies[0].getValue();
            User user = UserDAO.findUser(username);
            if (!username.equals("") && user.getRole().equals("admin")) {
                request.getRequestDispatcher("adminprofile.html").include(request, response);

                String number = request.getParameter("number");
                String airplaneType = request.getParameter("airplane-type");

                String departureCity_name = request.getParameter("departure-city");
                City departureCity = CityDAO.findCity(departureCity_name);


                String departureDateString = request.getParameter("departure-date");
                String departureTimeString = request.getParameter("departure-time");

                String arrivalCity_name = request.getParameter("arrival-city");
                City arrivalCity = CityDAO.findCity(arrivalCity_name);

                String arrivalDateString = request.getParameter("arrival-date");
                String arrivalTimeString = request.getParameter("arrival-time");

                Date departureDate = null;
                Date arrivalDate = null;
                Time departureTime = null;
                Time arrivalTime = null;

                try {
                    departureDate = new SimpleDateFormat("dd/MM/yyyy").parse(departureDateString);
                    arrivalDate = new SimpleDateFormat("dd/MM/yyyy").parse(arrivalDateString);
                    departureTime = new Time(new SimpleDateFormat("HH:mm").parse(departureTimeString).getTime());
                    arrivalTime = new Time(new SimpleDateFormat("HH:mm").parse(arrivalTimeString).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Flight flight = new Flight();

                flight.setNumber(number);
                flight.setAirplaneType(airplaneType);
                flight.setDepartureCity(departureCity);
                flight.setDepartureDate(departureDate);
                flight.setDepartureTime(departureTime);
                flight.setArrivalCity(arrivalCity);
                flight.setArrivalDate(arrivalDate);
                flight.setArrivalTime(arrivalTime);


                System.out.println(flight.getDepartureTime());
                System.out.println(flight.getArrivalTime());

                FlightDAO.addFlight(flight);

                response.sendRedirect("ViewFlightsServlet");
            } else if (!username.equals("") && user.getRole().equals("user")) {
                request.getRequestDispatcher("userprofile.html").include(request, response);
                out.println("<br/>You don't have enough privileges!");
            } else {
                out.println("Please login first!");
                request.getRequestDispatcher("index.html").include(request, response);
            }
        }

        out.close();

    }

}
