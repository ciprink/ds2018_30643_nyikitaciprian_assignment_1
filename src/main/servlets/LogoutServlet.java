package main.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LogoutServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Cookie cookie = new Cookie("name", "");
        cookie.setMaxAge(-1);
        response.addCookie(cookie);

        out.print("You are successfully logged out!");
        request.getRequestDispatcher("index.html").include(request, response);
        out.close();

    }

}
