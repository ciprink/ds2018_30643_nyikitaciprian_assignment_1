package main.servlets;

import main.dao.FlightDAO;
import main.dao.UserDAO;
import main.entities.Flight;
import main.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DeleteFlightServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            String username = cookies[0].getValue();
            User user = UserDAO.findUser(username);
            if (!username.equals("") && user.getRole().equals("admin")) {
                request.getRequestDispatcher("adminprofile.html").include(request, response);

                String sid = request.getParameter("id");
                int id = Integer.parseInt(sid);
                Flight flight = FlightDAO.findFlight(id);
                FlightDAO.deleteFlight(flight);
                response.sendRedirect("ViewFlightsServlet");

            } else if (!username.equals("") && user.getRole().equals("user")) {
                request.getRequestDispatcher("userprofile.html").include(request, response);
                out.println("<br/>You don't have enough privileges!");
            } else {
                out.println("Please login first!");
                request.getRequestDispatcher("index.html").include(request, response);
            }
        }

        out.close();

    }

}
