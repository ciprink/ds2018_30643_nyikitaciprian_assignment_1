package main.servlets;

import main.dao.UserDAO;
import main.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String username = request.getParameter("username");
        String password = request.getParameter("password");


        if( username == ""){
            request.getRequestDispatcher("index.html").include(request, response);
            out.println("Empty user");
        }

        else if (password == ""){
            request.getRequestDispatcher("index.html").include(request, response);
            out.println("Empty password");

        }

        else {

            User user = UserDAO.findUser(username);

            if (password.equals(user.getPassword())) {
                Cookie cookie = new Cookie("name", username);
                response.addCookie(cookie);
                if (user.getRole().equals("admin")) {
                    request.getRequestDispatcher("adminprofile.html").include(request, response);
                } else if (user.getRole().equals("user")) {
                    request.getRequestDispatcher("userprofile.html").include(request, response);
                }
            } else {
                out.print("Incorrect username or password!");
                request.getRequestDispatcher("index.html").include(request, response);
            }

        }

    }

}
