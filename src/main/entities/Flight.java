package main.entities;

import javax.persistence.ManyToOne;
import java.sql.Time;
import java.util.Date;

public class Flight {

    private int id;
    private String number;
    private String airplaneType;
    private City departureCity;
    private Date departureDate;
    private Time departureTime;
    private City arrivalCity;
    private Date arrivalDate;
    private Time arrivalTime;

    @ManyToOne
    private User user_fk;

    public Flight() {
    }

    public Flight(int id, String number, String airplaneType, City departureCity, Date departureDate, Time departureTime, City arrivalCity, Date arrivalDate, Time arrivalTime) {
        this.id = id;
        this.number = number;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureDate = departureDate;
        this.departureTime = departureTime;
        this.arrivalCity = arrivalCity;
        this.arrivalDate = arrivalDate;
        this.arrivalTime = arrivalTime;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }



    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureTime) {
        this.departureDate = departureTime;
    }

    public Time getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Time departureTime) {
        this.departureTime = departureTime;
    }


    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalTime) {
        this.arrivalDate = arrivalTime;
    }

    public Time getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Time arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
