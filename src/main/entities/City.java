package main.entities;

public class City {

    private int id;
    private String city_name;
    private Double latitude;
    private Double longitude;

    public City(int id, String city_name, Double latitude, Double longitude) {
        this.id = id;
        this.city_name = city_name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public City() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
