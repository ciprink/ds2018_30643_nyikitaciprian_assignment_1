package main.dao;

import main.entities.Flight;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class FlightDAO {

    public static void addFlight(Flight flight) {
        int flightId = -1;
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            flight.setId(flightId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public static Flight findFlight(int id) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    public static List<Flight> findFlights() {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flights;
    }

    public static void editFlight(Flight flight) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public static void deleteFlight(Flight flight) {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
